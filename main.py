from sys import stdout
from os.path import realpath, exists
from decimal import Decimal, getcontext, ROUND_HALF_UP
import signal
import argparse
import time
import asyncio
import aiohttp
import numpy as np
import pandas as pd
import pickle

STATE = [ 0,  pd.DataFrame() ]
SIGNALS = [ signal.SIGINT, signal.SIGTERM ]


def async_measure(template='{name}: {secs}'):
    def render_template(name, secs):
        print(template.format(name=name, secs=secs))

    def decorator(func):
        async def wrapper(*args, **kwargs):
            start = time.perf_counter()
            res = await func(*args, **kwargs)
            end = time.perf_counter()
            render_template(func.__name__, round(end - start, 2))   
            return res
        return wrapper
    return decorator


async def fetch(url, session):
    async with session.get(url) as response:
        return await response.json()


async def get_data_cr(url, limit, queue, queue_size, rate):
    async with aiohttp.ClientSession() as session:
        for _ in range(limit):
            try:
                results = await asyncio.gather(*[fetch(url, session) for _ in range(rate)])
                await asyncio.gather(*[queue.put(result) for result in results])
                while queue.qsize() > queue_size: 
                    await asyncio.sleep(0.1)
            except asyncio.CancelledError:
                raise


def load(store):
    global STATE
    filename = realpath(store)
    if not exists(filename):
        return [0, pd.DataFrame()]

    with open(filename, 'rb') as file:
        STATE = pickle.load(file)
    

def save(store):
    global STATE
    filename = realpath(store)
    with open(filename, 'wb') as file:
        pickle.dump(STATE, file)


def signal_handler(sig, frame):
    global SIGNALS
    global args
    if sig in SIGNALS:
        save(args.store)
        print(f'\n\nProcess inerrupted. Progress has been saved in {args.store}')
        exit(0)
    exit(sig)


@async_measure('La funcion {name} ha tardado {secs} segundos')
async def main(url, limit, precision, progress, rate, queue_size, store):
    getcontext().prec = precision + 1
    pd.set_option('display.max_rows', 10) 

    queue = asyncio.Queue()
    asyncio.create_task(get_data_cr(url, limit, queue, progress * 10, rate))

    def calc_prob(extractions):
        def fn(count):
            num = Decimal(count) / extractions
            prec = Decimal('0.' + ('0' * (precision - 1)) + '1')
            return num.quantize(prec, rounding=ROUND_HALF_UP)
        return fn

    def are_equals(prev, prev_count, curr, curr_count):
        if not prev.size or not curr.size or prev.size != curr.size:
            return False
        prob_1 = prev.copy()
        prob_1['count'] = prob_1['count'].apply(calc_prob(prev_count*curr.size))
        prob_2 = curr.copy()
        prob_2['count'] = prob_2['count'].apply(calc_prob(curr_count*curr.size))
        return prob_1.equals(prob_2)

    def patch(extractions):
        def fn(count):
            num = calc_prob(extractions)(count) * 256 / 30
            return num.quantize(Decimal('0.0001'), rounding=ROUND_HALF_UP)
        return fn

    load(store)
    global STATE
    count, curr = STATE
    count = int(count / progress)
    if count:
        print(f'Resumming from {count * progress}')

    while count * progress < limit:
        count += 1
        prev = curr if count > 1 else pd.DataFrame()

        try: 
            results = [await queue.get() for _ in range(progress)]
        except StopAsyncIteration:
            break

        words, counts = np.unique(results, return_counts=True)
        tmp = pd.DataFrame({'word': words, 'count': counts})
        tmp['count'] = tmp['count'].astype(int)
        
        if not curr.empty:
            merged = pd.concat([curr, tmp])
            curr = merged.groupby('word', as_index=False).sum()
        else:
            curr = tmp.copy()

        if are_equals(prev, progress * (count - 1), curr, progress * count):
            break

        print('.', end='')
        stdout.flush()
        STATE = [count * progress, curr]

    print('')
    count *= progress
    curr = curr.sort_values(by=['count', 'word'], ascending=[False, True])
    curr['count'] = curr['count'].apply(patch(count))
    return curr.reset_index(drop=True), count


if __name__ == '__main__':
    description = '''
    This script calaulates the probability of a word to appear in the array that returns
    the endpoint defined in the argument URL.
    It returns the 10 more probable words in csv format, and the word in the 46th position.
    '''
    epilog = '''
    This is a solution for the "Adjetivos revesros" challenge of the serie of challenges 
    "Tormenta de código" of Malandriner community:
    https://github.com/webreactiva-devs/tormenta-adjetivos-reversos
    '''
       
    parser = argparse.ArgumentParser(description=description, epilog=epilog)

    parser.add_argument('--url', default='http://localhost:3000', type=str,
        help='URL to explode. Default: http://localhost:3000'
    )
    parser.add_argument('--limit', default=100_000_000, type=int,
        help='Limit of requests to make. Default: 100_000_000'
    )
    parser.add_argument('--precision', default=8, type=int,
        help='Decimals to use in for 1-based probability calclulation. Default: 8'
    )
    parser.add_argument('--progress', default=1_000, type=int, 
        help='Number of requests to prompt advance. Default: 1_000'
    )
    parser.add_argument('--rate', default=300, type=int, 
        help='Optimal number of requests to make in the same time. Default: 300'
    )
    parser.add_argument('--queue-size', default=5_000, type=int,
        help='Size of requested data queue. Default: 5_000'
    )
    parser.add_argument('--store', default='store.bin', type=str,
        help='PKL file where save and load progress. Default: store'
    )
    args = parser.parse_args()

    for signum in SIGNALS:
        signal.signal(signum, signal_handler)

    result, num = asyncio.run(main(**args.__dict__))
    save(args.store)

    prob_sum = result['count'].sum()
    
    msgs = {
        'not_found': f'Not a convenient result has been found after {num} requests: {prob_sum}',
        'found': f'Result found in {num} requests: {prob_sum}',
    }

    print(
        result['word'].size,
        '\n',
        msgs['not_found'] if num == args.limit else msgs['found'],
        80 * '=',
        result.head(10).to_csv(index=False, header=False),
        80*'=',
        f'El adjetivo nº46 es: {result.loc[45, "word"]}',
        80 * '=',
        sep='\n'
    )
    
