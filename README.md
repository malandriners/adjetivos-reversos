# Tormenta de código: Adjetivos reversos

> 🎯 Propósito: Hacer ingeniería inversa a través del código

![DALL·E 2023-12-10 21 23 37 - An image in pixel art style symbolizing a group of adjectives hidden behind an invisible wall  The scene shows pixelated adjectives in various colors ](https://github.com/webreactiva-devs/tormenta-adjetivos-reversos/assets/1122071/a7855500-3cae-4ade-92c3-6e4efed4bdd2)


Este reto se realiza por y para la [Comunidad Malandriner](https://webreactiva.com/comunidad)

Toda la información del reto [aquí](https://github.com/webreactiva-devs/tormenta-adjetivos-reversos/tree/main).


## Método propuesta

Un script de python el cual tiene la siguiente ayuda:
```
usage: main.py [-h] [--url URL] [--limit LIMIT] [--precision PRECISION] [--progress PROGRESS] [--rate RATE] [--queue-size QUEUE_SIZE]
               [--store STORE]

This script calaulates the probability of a word to appear in the array that returns the endpoint defined in the argument URL. It
returns the 10 more probable words in csv format, and the word in the 46th position.

options:
  -h, --help            show this help message and exit
  --url URL             URL to explode. Default: http://localhost:3000
  --limit LIMIT         Limit of requests to make. Default: 100_000_000
  --precision PRECISION
                        Decimals to use in for 1-based probability calclulation. Default: 8
  --progress PROGRESS   Number of requests to prompt advance. Default: 1_000
  --rate RATE           Optimal number of requests to make in the same time. Default: 1_000
  --queue-size QUEUE_SIZE
                        Size of requested data queue. Default: 5_000
  --store STORE         File where save and load progress. Default: store

This is a solution for the "Adjetivos revesros" challenge of the serie of challenges "Tormenta de código" of Malandriner community:
https://github.com/webreactiva-devs/tormenta-adjetivos-reversos
```

La aplicación funciona de la siguiente manera:
1. Compueba los argumentos.
2. Lanza una tarea para que vaya descargandose resultados a una cola.
3. De mientras, carga el archivo donde se guarda el avance de las descargas, para no tener que empezar desde 0 cada vez que se queremos calcular, ya que la descarga de resultados es la parte que más tiempo consume.
4. Por cada grupo de resultados que se recuperan de la cola, se acumulan y se comparan con las probabilidades calculadas en la iternación anterior. Si las probabilidades no han cambiado, se da como correctas las probabilidades calculadas, y si sí que han cambiado, se recogen más datos para volver a calcular las probabilidades, hasta que se llege al limite de peticiones, definida en el argumento `LIMIT`.


### Solución

La solución dada con 30M de muestras, comparando las probabilidades con 8 decimales es: 

Número de adjetivos: `494`

Primeros 10 adjetivos
|Adjetivo|Probabilidad|
|:--|--:|
|Tranquilo|1.0008 |
|Responsable|1.0007|
|Conocido|1.0006|
|Ágil|0.9999|
|Estupendo|0.9999|
|Listo|0.9997|
|Caprichoso|0.9997|
|Estimado|0.9996|
|Grandioso|0.9996|
|Terrible|0.9994|

El adjetivo nº 46, es: `Sombrío`




## Método marrullero
Realizar ingeiería inversa al ejecutable proporcionado (usando Linux).

Para ello:
1. Lanzar el ejecutable proporcionado en segundo plano.
```
$ ./app-linuxX64.kexe &
```
2. Localizar y seleccionar el PID de uno de los subprocesos que esté sirviendo la aplicación, ya que el proceso principal suele usarse par coordeninar la carga y los subpreocesos.
```
$ pstree -p $(pidof app-linuxX64.kexe)
app-linuxX64.ke(468484)─┬─{app-linuxX64.ke}(468485)
                        ├─{app-linuxX64.ke}(468486)
                        ├─{app-linuxX64.ke}(468487)
                        ├─{app-linuxX64.ke}(468488)
                        ├─{app-linuxX64.ke}(468489)
                        ├─{app-linuxX64.ke}(468490)
                        ...
```
3. Realizar un volcado de memoria del subproceso que seleccionemos. Para esto necesitamos permisos elevados. Esto nos generará el archivo `core.468488` ( el número es el PID )
```
$ sudo gdb - p 468488
(gdb) generate-core-file
(gdb) detach
(gdb) quit
```

4. Localizar la información que buscamos. En este caso, como es una aplicacion de Kotlin, que se ejecuta en la maquina virtual de Java, los caracteres se guardarán en UTF16le.
Para que la búsqueda sea más fácil, buscaremos una cadena en la cual, el valor de sus caracteres en ascii, sean menores de 255, e intercalaremos un byte con valor 0 entre cada caracter.
Por ejemplo, pra buscar `Conocido` buscaremos `43 00 6F 00 6E 00 6F 00 63 00 69 00 64 00 6F 00`.
Para realizar esto puedes usar cualquier editor hexadecimal.
Así lo he realizado yo:
```
$ hexedit core.468488
```
```
00000000   7F 45 4C 46  02 01 01 00  00 00 00 00  00 00 00 00  .ELF............
00000010   04 00 3E 00  01 00 00 00  00 00 00 00  00 00 00 00  ..>.............
00000020   40 00 00 00  00 00 00 00  30 81 06 3B  01 00 00 00  @.......0..;....
00000030   00 00 00 00  40 00 38 00  CF 03 40 00  D1 03 D0 03  ....@.8...@.....
00000040   04 00 00 00  04 00 00 00  88 45 06 3B  01 00 00 00  .........E.;....
00000050   00 00 00 00  00 00 00 00  00 00 00 00  00 00 00 00  ................
00000060   8C 3B 00 00  00 00 00 00  00 00 00 00  00 00 00 00  .;..............
00000070   01 00 00 00  00 00 00 00  01 00 00 00  04 00 00 00  ................
00000080   88 D5 00 00  00 00 00 00  00 00 20 00  00 00 00 00  .......... .....
-%%  core.468488       --0x0/0x13B077570--0%-----------------------------------
```
pulsar `/` para buscar, introducir la cadna a buscar
```
00000000   7F 45 4C 46  02 01 01 00  00 00 00 00  00 00 00 00  .ELF............
00000010   04 00 3E 00  01 00 00 00  00 00 00 00  00 00 00 00  ..>.............
00000020   40 00 00 00  00 00 00 00  30 81 06 3B  01 00 00 00  @.......0..;....

                            Hexa string to search: 43 00 6F 00 6E 00 6F 00 63 00
 69 00 64 00 6F 00
00000060   8C 3B 00 00  00 00 00 00  00 00 00 00  00 00 00 00  .;..............
00000070   01 00 00 00  00 00 00 00  01 00 00 00  04 00 00 00  ................
00000080   88 D5 00 00  00 00 00 00  00 00 20 00  00 00 00 00  .......... .....
-%%  core.468488       --0x0/0x13B077570--0%-----------------------------------
```
 y pulsar enter
```
0012D6D0   20 00 22 00  6E 00 61 00  6D 00 65 00  22 00 3A 00   .".n.a.m.e.".:.
0012D6E0   20 00 22 00  43 00 6F 00  6E 00 67 00  65 00 6C 00   .".C.o.n.g.e.l.
0012D6F0   61 00 64 00  6F 00 22 00  2C 00 0A 00  20 00 20 00  a.d.o.".,... . .
0012D700   20 00 20 00  22 00 70 00  72 00 6F 00  62 00 22 00   . .".p.r.o.b.".
0012D710   3A 00 20 00  22 00 30 00  2E 00 36 00  34 00 38 00  :. .".0...6.4.8.
0012D720   32 00 30 00  22 00 0A 00  20 00 20 00  7D 00 2C 00  2.0."... . .}.,.
0012D730   0A 00 20 00  20 00 7B 00  0A 00 20 00  20 00 20 00  .. . .{... . . .
0012D740   20 00 22 00  6E 00 61 00  6D 00 65 00  22 00 3A 00   .".n.a.m.e.".:.
0012D750   20 00 22 00  43 00 6F 00  6E 00 6F 00  63 00 69 00   .".C.o.n.o.c.i.
-%%  core.468488       --0x12D754/0x13B077570--0%------------------------------
```
Ahora podemos reconocer la cadena del JSON. moviendonos arriba y abajo podemos ver el inicio de un array de diccionarios ( terminología de Python ) con claves `name` y `prob`.
En mi caso compienza en la posición `0x12A9D8`
```
0012A9D0   59 6B 00 00  00 00 00 00  5B 00 0A 00  20 00 20 00  Yk......[... . .
0012A9E0   7B 00 0A 00  20 00 20 00  20 00 20 00  22 00 6E 00  {... . . . .".n.
0012A9F0   61 00 6D 00  65 00 22 00  3A 00 20 00  22 00 41 00  a.m.e.".:. .".A.
0012AA00   62 00 69 00  73 00 6D 00  61 00 6C 00  22 00 2C 00  b.i.s.m.a.l.".,.
0012AA10   0A 00 20 00  20 00 20 00  20 00 22 00  70 00 72 00  .. . . . .".p.r.
0012AA20   6F 00 62 00  22 00 3A 00  20 00 22 00  30 00 2E 00  o.b.".:. .".0...
0012AA30   32 00 39 00  33 00 38 00  30 00 22 00  0A 00 20 00  2.9.3.8.0."... .
0012AA40   20 00 7D 00  2C 00 0A 00  20 00 20 00  7B 00 0A 00   .}.,... . .{...
0012AA50   20 00 20 00  20 00 20 00  22 00 6E 00  61 00 6D 00   . . . .".n.a.m.
-%%  core.468488       --0x12A9D8/0x13B077570--0%------------------------------
```
y termina en la posición `0x138088`
```
00138000   20 00 22 00  30 00 2E 00  35 00 35 00  39 00 33 00   .".0...5.5.9.3.
00138010   36 00 22 00  0A 00 20 00  20 00 7D 00  2C 00 0A 00  6."... . .}.,...
00138020   20 00 20 00  7B 00 0A 00  20 00 20 00  20 00 20 00   . .{... . . . .
00138030   22 00 6E 00  61 00 6D 00  65 00 22 00  3A 00 20 00  ".n.a.m.e.".:. .
00138040   22 00 56 00  75 00 6C 00  67 00 61 00  72 00 22 00  ".V.u.l.g.a.r.".
00138050   2C 00 0A 00  20 00 20 00  20 00 20 00  22 00 70 00  ,... . . . .".p.
00138060   72 00 6F 00  62 00 22 00  3A 00 20 00  22 00 30 00  r.o.b.".:. .".0.
00138070   2E 00 39 00  35 00 31 00  35 00 33 00  22 00 0A 00  ..9.5.1.5.3."...
00138080   20 00 20 00  7D 00 0A 00  5D 00 00 00  00 00 00 00   . .}...].......
-%%  core.468488       --0x138088/0x13B077570--0%------------------------------
```

5. Extraer en otro archivoese bloque de bytes.
   Desde 0x12A9D8 (1223128 en decimal) hasta 0x138088 (1278088 en decimal).
   Al final, le agregaremos 2 bytes, el tamaño de una letra en utf16le, para poder recoger también el valor del texto alacenado en esa posición.
   Así pues recuperamos los primeros 54962 bytes ( 1278090 - 1223138 ) desde la posicion 1223138.
```
$ dd if=core.468490 of=adjetivos.json skip=1223128 bs=1 count=54962
```

Y así obtendríamos, de un modo un pelín marrullero, el json con las probabilidades de partida con los que el programa debería servir los adjetivos.



### NOTAS IMPORTANTES DEL MODO MARRULLERO

Esto ha sido posible en este caso, porque el json con los adjuetivos y las probabilidades estaban alacenadas en memoria, y ésta no se ha limpiado después de parsearla.

OJO que no quiere decir que estos datos den las respuestas correctas, ya que algún proceso posterior podría modificar las probabilidades desde que se parsea el json hasta que se decide con qué probabilidad elegir las palabras.

Por ejemplo, por el metodo propuesto, con de 30M de muestras se contabilizan 494 adjetivos, mientras que por el método marrullero, se contabilizan 499.

Esto es sólo ilustrativo, y para incordiar un poco. No lo uses para hacer el mal.


